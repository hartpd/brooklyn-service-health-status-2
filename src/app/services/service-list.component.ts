import { Component, OnInit } from '@angular/core';

import { Service } from './service';
import { ServiceService } from './service.service';

@Component({
    templateUrl: './service-list.component.html'
})

export class ServiceListComponent implements OnInit {
    title: string = 'Brooklyn Service Health Status';
    errorMessage: string;
    
    services: Service[];

    constructor(private _serviceService: ServiceService) { }

    ngOnInit(): void {
        this._serviceService.getServices()
                .subscribe(services => this.services = services,
                           error => this.errorMessage = <any>error);
    }
}
